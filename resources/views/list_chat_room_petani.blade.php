@extends('template.template')
@section('content')
@php
$user= Session::get("user");
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="row">
                    <div class="col-md-8">
                      <h5>List Chat Room</h5>
                      <ul id="data">
                      </ul>
                      <a id="buatChatBaru" href="{{route('create_room_petani')}}" disabled>Buat Chat Baru</a>

                       
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
<script>
$(document).ready(function(){
  $.ajax({
        url: 'https://api.talasi.codr-staging.id/farmer/chat',
        type: 'GET',
        beforeSend: function(xhr){
            xhr.setRequestHeader('Authorization', 'Bearer 80cc562bc-8e19-4206-8ebd-a12d7df964ce');
        },
        success: function(response){
            var ulElement = $('<ul></ul>');
            
            var enableBuatChatBaru = true;

            $.each(response.data, function(index, item) {
                var itemId = item.id;
                
                var liElement = $('<li></li>');
                var anchorElement = $('<a></a>')
                    .text(item.code_room); // Ganti 'item.code_room' dengan properti yang sesuai dalam respons Anda

                if (item.is_active_flag === 'Y') {
                    anchorElement.attr('href', "{{ route('home_petani', 'itemId') }}".replace('itemId', itemId));
                } else {
                    enableBuatChatBaru = false;
                }
                
                liElement.append(anchorElement);
                
                ulElement.append(liElement);
            });

            $('#data').append(ulElement);

            if (enableBuatChatBaru) {
                $('#buatChatBaru').prop('disabled', false);
            }
        },
        error: function(xhr, status, error){
            console.log('Error: ' + error);
        }
    });
});

</script>

@endpush
@endsection