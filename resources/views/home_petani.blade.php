@extends('template.template')
@section('content')
@php
$user= Session::get("user");
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="row">
                    <h1>Room Code : {{$id}}</h1>
                    <div class="col-md-8">
                        <h3>Chat</h3>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" name="username" id="username" value="{{$user["name"]}}" class="form-control" autocomplete="off" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="message">Message:</label>
                            <input type="text" name="message" id="message" class="form-control" autocomplete="off"/>
                        </div>
                        <a href="javascript:void(0);" class="btn btn-outline-secondary mt-3" id="btn-send">Send Message</a>
                        <input type="hidden" name="action" id="action"/>
                        <hr/>
                        <input type="text" id="m">
            
                        <div id="status" class="mt-2 float-end">
            
                        </div>
                        <div id="output" class="chatbox">
                            
                        </div>
                    </div>
                    <div class="col-md-4">
                        Who's online
                        <ul id="online_users"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.slim.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.js"></script>
<script>
const offline = `<span class="badge bg-danger">Not connected</span>`
const online = `<span class="badge bg-success">Connected</span>`
let statusDiv = document.getElementById("status");

var socket = io("wss://websocket.talasi.codr-staging.id");
  var userId = parseInt({{$user["id"]}}, 10); // 10 adalah basis bilangan (base) yang digunakan, biasanya 10 untuk desimal.
  
  var chatRoomId = parseInt({{$id}}, 10);
  let username= $("#username").val();
  var outputDiv = $("#output");

  socket.on('disconnect', function(){
    console.log("disconnect");
    statusDiv.innerHTML = offline;
  });

  socket.on('connect', function() {
    console.log("connect");

        // saat terkoneksi maka akan request join room
        socket.emit("joinRoomFarmer",{
            "user_id":userId,
            "chat_room_id":chatRoomId,
            "name":username,
        });
        statusDiv.innerHTML = online;

        // setelah join room wajib memgirimkan request empty message
        socket.emit("Chat", {
            "user_id":userId,
            "chat_room_id":chatRoomId,
            "name":username,
            "message":"" // empty message
        });

  });

    socket.emit("joinRoomFarmer",{
        "user_id":userId,
        "chat_room_id":chatRoomId,
        "name":username,
    });

    socket.on("reply-join",function(msg){
        console.log("halo reply ", msg);
        // Mengecek apakah elemen #output sudah ada
        if (outputDiv.length === 0) {
            // Jika tidak ada, maka buat elemen #output terlebih dahulu
            outputDiv = $("<div>").attr("id", "output").addClass("chatbox");
            $("body").append(outputDiv);
        }

        // Mengulangi setiap pesan dalam array msg.message
        for (var i = 0; i < msg.message.length; i++) {
            var alertDiv = $("<div>").addClass("alert").attr("role", "alert");
            alertDiv.text(msg.message[i].message);

            if (msg.message[i].user_id === userId) {
                alertDiv.addClass("alert-primary");
            } else {
                alertDiv.addClass("alert-danger");
            }

            outputDiv.append(alertDiv);
        }
    })

    socket.on('reply-chat', function(msg) {
        console.log("reply-chat ", msg);
        var messageDiv = $("<div>").addClass("alert").attr("role", "alert");
        messageDiv.text(msg.message);

        if (msg.user_id === userId) {
            messageDiv.addClass("alert-primary");
        } else {
            messageDiv.addClass("alert-danger");
        }

        $("#output").append(messageDiv);
            // Scroll to the bottom of the output div
        var outputDiv = document.getElementById("output");
        outputDiv.scrollTop = outputDiv.scrollHeight;

    });





//   socket.on("reply", function (msg) {
//     var outputDiv = $("#output");

//     // Mengecek apakah elemen #output sudah ada
//     if (outputDiv.length === 0) {
//         // Jika tidak ada, maka buat elemen #output terlebih dahulu
//         outputDiv = $("<div>").attr("id", "output").addClass("chatbox");
//         $("body").append(outputDiv);
//     }

//     // Mengulangi setiap pesan dalam array msg.message
//     for (var i = 0; i < msg.message.length; i++) {
//         var alertDiv = $("<div>").addClass("alert").attr("role", "alert");
//         alertDiv.text(msg.message[i].message);

//         if (msg.message[i].user_id === userId) {
//             alertDiv.addClass("alert-primary");
//         } else {
//             alertDiv.addClass("alert-danger");
//         }

//         outputDiv.append(alertDiv);
//     }
//   });


//   socket.on("reply-chat", function (msg) {
//     console.log("Log reply-chat", msg);
//     var messageDiv = $("<div>").addClass("alert").attr("role", "alert");
//     messageDiv.text(msg.message);

//     if (msg.user_id === userId) {
//         messageDiv.addClass("alert-primary");
//     } else {
//         messageDiv.addClass("alert-danger");
//     }

//     $("#output").append(messageDiv);
//         // Scroll to the bottom of the output div
//     var outputDiv = document.getElementById("output");
//     outputDiv.scrollTop = outputDiv.scrollHeight;
//   })

//   socket.on("error", function (msg) {
//     console.log("error websocket ", msg);
//   });




//   socket.on("chat", function(msg){
//     console.log("halo chat", msg);
//   })


  $("#btn-send").click(function() {
       let message=$("#message").val();
       console.log("button send ditekan",message);
        socket.emit("Chat", {
            "user_id":userId,
            "chat_room_id":chatRoomId,
            "name":username,
            "message":message,
            "offer_id":0,
            "coupon_id":0
        });
      
      $("#message").val("");

    })



  // $("form").submit(function () {
  //   s2.emit("msg", $("#m").val(), function (data) {
  //     $("#messages").append($("<li>").text("ACK CALLBACK: " + data));
  //   });

  //   socket.emit("notice", $("#m").val());

  //   $("#m").val("");
  //   return false;

  // });
</script>
@endpush
@endsection