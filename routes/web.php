<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('login.login');
});

Auth::routes();

Route::get("/login_internal", [App\Http\Controllers\Auth\LoginController::class, 'showLoginFormPetani']);
Route::post('/login_internal', [App\Http\Controllers\Auth\LoginController::class, 'loginInternal'])->name('login_internal');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home-petani/{id}', [App\Http\Controllers\HomeController::class, 'indexPetani'])->name('home_petani');
Route::get('list-chat', [App\Http\Controllers\HomeController::class, 'ListChatPetani'])->name('list_chat_petani');

Route::get('create-room', [App\Http\Controllers\HomeController::class, 'createRoom'])->name('create_room_petani');
