<?php

namespace App\Http\Curl;

class CurlHelper
{
    public static function get($url, $headers = [])
    {
        return self::request('GET', $url, [], $headers);
    }

    public static function post($url, $data = [], $headers = [])
    {
        return self::request('POST', $url, $data, $headers);
    }

    private static function request($method, $url, $data = [], $headers = [])
    {
        $ch = curl_init();

        if ($method === 'GET' && !empty($data)) {
            $url .= '?' . http_build_query($data);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($method === 'POST') {

            $data = json_encode($data); // Ubah data menjadi format JSON
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json', // Tentukan tipe konten sebagai JSON
                'Content-Length: ' . strlen($data) // Tentukan panjang konten
            ]);
        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);

        if (curl_errno($ch)) {

            // Handle error here
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Mendapatkan kode status HTTP


        curl_close($ch);

        return ['http_code' => $httpCode, 'response' => $response];
    }
}
