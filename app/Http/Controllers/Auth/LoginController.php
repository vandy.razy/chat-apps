<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Curl\CurlHelper;
use App\Providers\RouteServiceProvider;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if (Session::has('user')) {

            return redirect()->route('home');
        }
        return view('login.login');
    }
    public function login(Request $request)
    {


        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $endpointUrl = 'https://api.talasi.codr-staging.id/auth/login-farmer';

        $data = ['email_or_phone' => $request->email, "password" => $request->password];

        $response = CurlHelper::post($endpointUrl, $data, []);

        if ($response['http_code'] != 200) {
            return redirect()->back()->with('error', $response['response']);
        }

        Session::forget('user');
        $userData = json_decode($response['response'], true);
        $this->setAuthenticatedUser($userData['data']); // Anda perlu menyesuaikan dengan struktur responsenya

        return redirect()->route('list_chat_petani');
    }


    public function showLoginFormPetani()
    {

        return view('login.login_internal');
    }


    public function loginInternal(Request $request)
    {


        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $endpointUrl = 'https://api.talasi.codr-staging.id/auth/login-internal-website';

        $data = ['email_or_phone' => $request->email, "password" => $request->password];

        $response = CurlHelper::post($endpointUrl, $data, []);

        if ($response['http_code'] != 200) {
            return redirect()->back()->with('error', $response['response']);
        }

        Session::forget('user');
        $userData = json_decode($response['response'], true);
        $this->setAuthenticatedUser($userData['data']); // Anda perlu menyesuaikan dengan struktur responsenya

        return redirect()->route('home');
    }

    protected function setAuthenticatedUser($userData)
    {
        // Simpan data pengguna ke dalam session
        Session::put('user', $userData);
    }
}
