<?php

namespace App\Http\Controllers;

use App\Http\Curl\CurlHelper;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (!Session::has('user')) {

            return redirect()->route('login');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data = Session::get('user');
        return view('home', compact('data'));
    }

    public function indexPetani($id)
    {


        $data = Session::get('user');
        return view('home_petani', compact('data', 'id'));
    }

    public function ListChatPetani()
    {
        return view('list_chat_room_petani');
    }

    public function createRoom()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.talasi.codr-staging.id/farmer/chat/create-room', // Perhatikan penambahan "http://" pada URL
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer 80cc562bc-8e19-4206-8ebd-a12d7df964ce'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $responseArray = json_decode($response, true);

        // Memeriksa apakah deserialisasi berhasil
        if (json_last_error() === JSON_ERROR_NONE) {
            // Deserialisasi berhasil
            $data = $responseArray["data"];
            $id = $responseArray["data"]["id"];
            return redirect()->route('home_petani', $id);
        } else {
            // Deserialisasi gagal
            echo "Gagal mengurai JSON: " . json_last_error_msg();
        }
    }
}
